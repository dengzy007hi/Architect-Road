package com.dzy.system.rbadmin.components.dydatasource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class DynamicDataSourceContextHolder {

    private static final Logger LOGGER = LoggerFactory.getLogger(DynamicDataSourceContextHolder.class);

    private static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();

    public static List<Object> dataSourceIds = new ArrayList<Object>();
 
    public static void setDataSourceType(String dataSourceType) {
        contextHolder.set(dataSourceType);
    }
 
    public static String getDataSourceType() {
        return contextHolder.get();
    }
 
    public static void clearDataSourceType() {
        contextHolder.remove();
    }
 
    /**
     * 判断指定DataSrouce当前是否存在
     * @param dataSourceId
     * @return
     * @author dengzy
     * @create  2017年7月7日
     */
    public static boolean containsDataSource(String dataSourceId){
        return dataSourceIds.contains(dataSourceId);
    }
}