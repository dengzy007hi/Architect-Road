package com.dzy.system.rbadmin.components.dydatasource;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 *
 * Created by dzy20 on 2017-07-07.
 */
public class DynamicDataSource extends AbstractRoutingDataSource{
    @Override
    protected Object determineCurrentLookupKey() {
        return null;
    }
}
